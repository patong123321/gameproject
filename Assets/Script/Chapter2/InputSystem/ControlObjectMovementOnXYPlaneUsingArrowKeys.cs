﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace Patipat.GameDev3.Chapter2.InputSystem
{
    public class ControlObjectMovementOnXYPlaneUsingArrowKeys : MonoBehaviour
    {
        public float m_MovementStep;

        void Start()
        {

        }

        void Update()
        {
            Keyboard keyboard = Keyboard.current;
            if (keyboard[Key.LeftArrow].isPressed)
            {
                this.transform.Translate(-m_MovementStep, 0, 0);
            }
            else if (keyboard[Key.RightArrow].isPressed)
            {
                this.transform.Translate(m_MovementStep, 0, 0);
            }
            else if (keyboard[Key.UpArrow].isPressed)
            {
                this.transform.Translate(0, m_MovementStep, 0);
            }
            else if (keyboard[Key.DownArrow].isPressed)
            {
                this.transform.Translate(0, -m_MovementStep, 0);
            }
        }
    }
}  