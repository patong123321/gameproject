using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Patipat.GameDev3.Chapter1;
using System.Diagnostics;

namespace Patipat.GameDev3.Chapter4
{
    public class PlayerTriggerWithITC : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            //Get components from item object
            //Get the ItemTypeComponent component from the triggered object
            ItemTypeComponent itc = other.GetComponent <ItemTypeComponent >();
        
            //Get components from the player
            //Inventory
            var inventory = GetComponent <Inventory >();
            //SimpleHealthPointComponent
            var simpleHP = GetComponent <SimpleHealthPointComponent >();
        
            if (itc !=null)
            {
                switch (itc.Type)
                {
            
                    case ItemType.COIN:
                        inventory.AddItem("COIN",1);
                        break;
                    case ItemType.BIGCOIN:
                        inventory.AddItem("BIGCOIN",1);
                        break;
                    case ItemType.POWERUP:
                        inventory.AddItem("POWERUP",1);
                        if(simpleHP != null)
                            simpleHP.HealthPoint = simpleHP.HealthPoint + 10; 
                        break;
                    case ItemType.POWERDOWN:
                        inventory.AddItem("POWERDOWN",1);
                        if(simpleHP != null)
                            simpleHP.HealthPoint = simpleHP.HealthPoint - 10;
                        break;
                    case ItemType.REGULARCOIN:
                        inventory.AddItem("REGULARCOIN",1);
                        break;
                    case ItemType.UNREGULARCOIN:
                        inventory.AddItem("UNREGULARCOIN",1);
                        if(simpleHP != null)
                            simpleHP.HealthPoint = simpleHP.HealthPoint - 20;
                        break;
                }
            }
        
        
            Destroy(other.gameObject ,0);
        }
    }
}