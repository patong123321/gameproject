using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    public Transform CameraPosition;
    
    public void Update()
    {
        transform.position = CameraPosition.position;
    }
    
}
